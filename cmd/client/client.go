package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi13/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi13Client struct {
	pb.Gogi13Client
}

func NewGogi13Client(address string) *Gogi13Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi13 service", l.Error(err))
	}

	c := pb.NewGogi13Client(conn)

	return &Gogi13Client{c}
}
